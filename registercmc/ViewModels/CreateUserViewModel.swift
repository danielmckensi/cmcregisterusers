//
//  CreateUserViewModel.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation
class CreateUserViewModel{
    
    private let usersManager = UsersManager.get
    var listenerUserCreated: ((GenericRes) -> Void)?
      var listenerOnFailure: (() -> Void)?

    func createUser(cedula: String, celular: String, direccion: String, email: String, nombre: String){
        if let responseValue = listenerUserCreated {
            usersManager.createUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre, responseValue: responseValue, onFailure: listenerOnFailure)
        }
    }
    
    func editUser(cedula: String, celular: String, direccion: String, email: String, nombre: String){
        if let responseValue = listenerUserCreated {
            usersManager.editUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre, responseValue: responseValue)
        }
    }
    
}
