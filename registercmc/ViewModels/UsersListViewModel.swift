//
//  UsersListViewModel.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class UserListViewModel{
    private let usersManager = UsersManager.get
    var listenerUserList: (([UserRes]) -> Void)?
    var listenerOnFailure: (() -> Void)?
    
    func requestUserList() {
           if let responseValue = listenerUserList {
            usersManager.getUsersList(responseValue: responseValue, onFailure: listenerOnFailure)
           }
       }
}
