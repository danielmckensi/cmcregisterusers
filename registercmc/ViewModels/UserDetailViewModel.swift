//
//  UserDetailViewModel.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class UserDetailViewModel{
    private let usersManager = UsersManager.get
    var listenerUser: ((UserRes) -> Void)?
    var listenerDeleteUser : ((GenericRes) -> Void)?
    var listenerError : (() -> Void)?

        
    
    func getUser(cedula: String){
         if let responseValue = listenerUser {
             usersManager.getUser(cedula: cedula, responseValue: responseValue, onFailure: listenerError)
         }
     }
    
    func deleteUser(cedula: String){
        if let responseValue = listenerDeleteUser{
            usersManager.deleteUser(cedula: cedula, responseValue: responseValue, onFailure: listenerError)
        }
    }
}
