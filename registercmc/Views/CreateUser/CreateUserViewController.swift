//
//  CreateUserViewController.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 8/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import NotificationBannerSwift
import SVProgressHUD

class CreateUserViewController: UIViewController {
    
    @IBOutlet weak var txtNames: UITextField!
    @IBOutlet weak var txtDocument: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtDirection: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSave: UIButton!
    
    var user : UserRes?
    var viewModel = CreateUserViewModel()
    var isEditingUser : Bool = false
    
    init(){
        super.init(nibName: "CreateUserViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isEditingUser {
            self.title = "Editar usuario"
        }else{
            self.title = "Crear usuario"
        }
        setupKeyboard()
        initListener()
        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func initListener(){
        viewModel.listenerUserCreated = { [weak self] messageResponse in
            print(messageResponse.message ?? "")
            let messageCreated = "Usuario creado exitosamente"
            let messageEdited = "Usuario editado exitosamente"
            let alert = UIAlertController(title: "Exito", message: self?.isEditingUser ?? false ? messageEdited: messageCreated, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in
                self?.goBack()
            }
            alert.addAction(alertAction)
            SVProgressHUD.dismiss()
            self?.present(alert, animated: true)
        }
        
        viewModel.listenerOnFailure = {
            let banner = NotificationBanner(title: "Error", subtitle: "Ocurrio un problema", style: .danger)
            banner.show()
            SVProgressHUD.dismiss()
        }
    }
    
    func requestCreateUser(){
        let cedula = txtDocument.text ?? ""
        let celular = txtPhone.text ?? ""
        let direccion = txtDirection.text ?? ""
        let email = txtEmail.text ?? ""
        let nombre = txtNames.text ?? ""
        viewModel.createUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre)
        SVProgressHUD.show()
    }
    
    func editUserCreateUser(){
        let cedula = txtDocument.text ?? ""
        let celular = txtPhone.text ?? ""
        let direccion = txtDirection.text ?? ""
        let email = txtEmail.text ?? ""
        let nombre = txtNames.text ?? ""
        viewModel.editUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre)
        SVProgressHUD.show()
    }
    
    func setUpUI(){
        
        if isEditingUser {
            txtDocument.isEnabled = false
            txtDocument.textColor = UIColor.systemGray2
        }else{
            txtDocument.isEnabled = true
        }
        
        txtNames.text = user?.nombre ?? ""
        if let cedula = user?.cedula {
            txtDocument.text = ("\(cedula)")
        }
        
        if let celular = user?.celular {
            txtPhone.text = ("\(celular)")
        }
        
        txtDirection.text = user?.direccion ?? ""
        txtEmail.text = user?.email ?? ""
    }
    
    func deleteUserReq(){
        
    }
    
    private func setupKeyboard() {
         IQKeyboardManager.shared.enable = false
         IQKeyboardManager.shared.enabledDistanceHandlingClasses.append(CreateUserViewController.self)
         IQKeyboardManager.shared.enabledToolbarClasses.append(CreateUserViewController.self)
         IQKeyboardManager.shared.enabledTouchResignedClasses.append(CreateUserViewController.self)
     }
    
    @IBAction func actionSave(_ sender: Any) {
        if(isEditingUser == true) {
            editUserCreateUser()
        }else{
            requestCreateUser()
        }
    }
    
}
