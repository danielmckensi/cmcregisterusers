//
//  UsersListViewController.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 8/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SVProgressHUD

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var tableViewUsers: UITableView!
    var viewModel = UserListViewModel()
    var users : [UserRes]? {
        didSet{
            tableViewUsers.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Usuarios"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addUser))
        registerCells()
        initListener()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        viewModel.requestUserList()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    init(){
        super.init(nibName: "UsersListViewController", bundle: nil)
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initListener(){
        viewModel.listenerUserList = { [weak self] users in
            self?.users = users
            SVProgressHUD.dismiss()
        }
        
        viewModel.listenerOnFailure = { 
            let banner = NotificationBanner(title: "Error", subtitle: "Ocurrio un problema", style: .danger)
            banner.show()
            SVProgressHUD.dismiss()
            
        }
    }
    
    func registerCells(){
        tableViewUsers.delegate = self
        tableViewUsers.dataSource = self
        tableViewUsers.registerNib("UsersListTableViewCell")
        
    }
    
    @objc func addUser(){
        let vc = CreateUserViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


extension UsersListViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension UsersListViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersListTableViewCell") as? UsersListTableViewCell
        cell?.lblUserName.text = users?[indexPath.row].nombre
        return cell ?? UITableViewCell() 
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UserDetailViewController()
        vc.user = users?[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}



extension UITableView {
    func setEmptyMessage(message:String) {
        let rect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
        let messageLabel = UILabel(frame: rect)
        messageLabel.text = message
        messageLabel.textColor = UIColor.darkGray
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = .center;
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()
        self.backgroundView = messageLabel;
        self.separatorStyle = .none;
    }
    
    func removeEmptyMessage() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
    enum ViewName: String {
        case noVisits = "VisitsHistoryEmpty"
    }
    
    func setEmptyView(withName name: ViewName) {
        let view = UINib(nibName: name.rawValue, bundle: .main).instantiate(withOwner: nil, options: nil).first as! UIView
        view.frame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.bounds.size.width, height: self.bounds.size.height))
        self.backgroundView = view
        self.separatorStyle = .none;
    }
    
    func removeEmptyView() {
        removeEmptyMessage()
    }
    
    func registerNib(_ nibName: String) {
        let cellNib = UINib.init(nibName: nibName, bundle: nil)
        register(cellNib, forCellReuseIdentifier: nibName)
    }
    
    func register(cell nib: String, withId id: String = "") {
        register(UINib(nibName: nib, bundle: nil), forCellReuseIdentifier: id.isEmpty ? nib : id)
    }
    
    func scrollToBottom(animated: Bool) {
        if self.numberOfRows(inSection: 0) > 0 {
            self.scrollToRow(at: NSIndexPath(row: self.numberOfRows(inSection: 0) - 1, section: 0) as IndexPath, at: .bottom, animated: animated)
        }
        self.layoutIfNeeded()
    }
}
