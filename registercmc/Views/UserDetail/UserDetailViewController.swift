//
//  UserDetailViewController.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 8/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import NotificationBannerSwift
import SVProgressHUD

class UserDetailViewController: UIViewController {
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblDocumentUser: UILabel!
    @IBOutlet weak var lblCelUser: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var lblEmailUser: UILabel!
    
    var user : UserRes?
    
    var viewModel = UserDetailViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        initListener()
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.show()
        requestUser()
    }
    
    init(){
        super.init(nibName: "UserDetailViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func initListener(){
        viewModel.listenerUser = { [weak self] user in
            self?.user = user
            self?.setUpUI()
            SVProgressHUD.dismiss()
        }
        
        viewModel.listenerDeleteUser = { [weak self] response in
            let alert = UIAlertController(title: "Exito", message: "Usuario borrado exitosamente", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in
                self?.goBack()
            }
            alert.addAction(alertAction)
            SVProgressHUD.dismiss()
            self?.present(alert, animated: true)
            
        }
        
        viewModel.listenerError = {
            let banner = NotificationBanner(title: "Error", subtitle: "Ocurrio un problema", style: .danger)
            SVProgressHUD.dismiss()
            banner.show()
        }
    }
    
    func setUpUI(){
        lblUserName.text = user?.nombre ?? ""
        lblCelUser.text = ("Celular: \(user?.celular ?? 0)")
        lblEmailUser.text = ("Email: \(user?.email ?? "")")
        lblDocumentUser.text =  ("Cedula: \(user?.cedula ?? 0)")
        lblUserAddress.text = ("Dirección: \(user?.direccion ?? "")")
    }
    
    func requestUser(){
        let cedula =  user?.cedula
        viewModel.getUser(cedula: ("\(cedula ?? 0)"))
    }
    
    func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func actionEdit(_ sender: Any) {
        let vc = CreateUserViewController()
        vc.user = user
        vc.isEditingUser = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionDelete(_ sender: Any) {
        
        let alert = UIAlertController(title: "Cuidado", message: "¿Desear borrar este usuario?", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Aceptar", style: .default) { (_) in
            self.viewModel.deleteUser(cedula: ("\(self.user?.cedula ?? 0)"))
        }
        
        let cancelAction = UIAlertAction(title: "Cancelar", style: .default) { (_) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(alertAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true)
        
        
    }
    
}
