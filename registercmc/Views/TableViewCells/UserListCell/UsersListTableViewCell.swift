//
//  UsersListTableViewCell.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit

class UsersListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
