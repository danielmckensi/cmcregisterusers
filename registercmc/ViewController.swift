//
//  ViewController.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 8/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import FirebaseAuth
import NotificationBannerSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var txtUserEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboard()
    }
    
    
    @IBAction func actionContinue(_ sender: Any) {
        Auth.auth().signIn(withEmail: txtUserEmail.text ?? "", password: txtPassword.text ?? "") { [weak self] authResult, error in
            guard let strongSelf = self else { return }
            if error != nil{
                let banner = NotificationBanner(title: "Error", subtitle: error?.localizedDescription, style: .danger)
                banner.show()
            }else{
                let vc = UsersListViewController()
                strongSelf.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
        
    }
    
    private func setupKeyboard() {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enabledDistanceHandlingClasses.append(ViewController.self)
        IQKeyboardManager.shared.enabledToolbarClasses.append(ViewController.self)
        IQKeyboardManager.shared.enabledTouchResignedClasses.append(ViewController.self)
    }
    
}

