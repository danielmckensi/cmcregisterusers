//
//  Api.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

struct UsersApi {
    private init(){
        
    }
    
    static let getUsersList = "https://prubascmc.herokuapp.com/usuario"
    static let createUser = "https://prubascmc.herokuapp.com/usuario?"
    static let editUser = "https://prubascmc.herokuapp.com/usuario/%@"
    static let getUser = "https://prubascmc.herokuapp.com/usuario/%@"
    static let deleteUser = "https://prubascmc.herokuapp.com/usuario/%@"
}
