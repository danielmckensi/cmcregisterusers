//
//  UserListRes.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class UserRes : Codable{
    var cedula: Int?
    var nombre: String?
    var celular: Int?
    var direccion: String?
    var email: String?
}
