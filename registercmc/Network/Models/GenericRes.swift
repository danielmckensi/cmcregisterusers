//
//  CreateUserRes.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation

class GenericRes: Codable {
    var message: String?
}
