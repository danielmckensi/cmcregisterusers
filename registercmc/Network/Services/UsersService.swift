//
//  GetUsers.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation
import Alamofire

class UserServices {
    
    static let get = UserServices()
    
    private init(){
        
    }
    
    
    func getUser(cedula: String, responseValue: @escaping (UserRes) -> Void, onFailure: (() -> Void)? = nil) {
        let url = String(format: UsersApi.editUser, cedula)
        ApiAdapter.get.requestGeneric(url: url, method: Alamofire.HTTPMethod.get) { (response: AFDataResponse<UserRes>) in
            if let response = response.value {
                responseValue(response)
            } else {
                onFailure?()
                print("getUsersError error -> \(response.error)")
            }
        }
    }
    
    
    
    func getUsersList(responseValue: @escaping ([UserRes]) -> Void, onFailure: (() -> Void)? = nil) {
        ApiAdapter.get.requestGeneric(url: UsersApi.getUsersList) { (response: AFDataResponse<[UserRes]>) in
            if let users = response.value {
                responseValue(users)
            } else {
                print("getUsersError error -> \(response.error)")
                onFailure?()
            }
        }
    }
    
    
    func createUser(cedula: String, celular: String, direccion: String, email: String, nombre: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil) {
        let queryParams = ["cedula": cedula, "celular": celular, "direccion": direccion, "email": email, "nombre": nombre]
        ApiAdapter.get.requestGeneric(url: UsersApi.createUser, method: Alamofire.HTTPMethod.post, queryParams: queryParams) { (response: AFDataResponse<GenericRes>) in
            if let status = response.response?.statusCode {
                let generic = GenericRes()
                if status == 201 {
                    generic.message = "Usuario creado exitosamente"
                    responseValue(generic)
                }else{
                    generic.message = "Ocurrio un error"
                    onFailure?()
                }
            } else {
                print("getUsersError error -> \(response.error)")
                onFailure?()
            }
        }
    }
    
    func editUser(cedula: String, celular: String, direccion: String, email: String, nombre: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil) {
        let queryParams = ["celular": celular, "direccion": direccion, "email": email, "nombre": nombre]
        let url = String(format: UsersApi.editUser, cedula)
        ApiAdapter.get.requestGeneric(url: url, method: Alamofire.HTTPMethod.put, queryParams: queryParams) { (response: AFDataResponse<GenericRes>) in
            if let status = response.response?.statusCode {
                let responseGeneric = GenericRes()
                if status == 200 {
                    responseGeneric.message = "Usuario editado exitosamente"
                }else{
                    responseGeneric.message = "Ocurrio un error"
                    onFailure?()
                }
                responseValue(responseGeneric)
            } else {
                print("getUsersError error -> \(response.error)")
                onFailure?()
            }
        }
    }
    
    func deleteUser(cedula: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil) {
        let url = String(format: UsersApi.deleteUser, cedula)
        ApiAdapter.get.requestGeneric(url: url, method: Alamofire.HTTPMethod.delete) { (response: AFDataResponse<GenericRes>) in
            if let status = response.response?.statusCode {
                let createUserRes = GenericRes()
                if status == 200 {
                    createUserRes.message = "Usuario editado exitosamente"
                }else{
                    createUserRes.message = "Ocurrio un error"
                    onFailure?()
                }
                responseValue(createUserRes)
            } else {
                print("getUsersError error -> \(response.error)")
                onFailure?()
            }
        }
    }
}
