//
//  UsersManager.swift
//  registercmc
//
//  Created by Daniel Steven Murcia Almanza on 9/07/20.
//  Copyright © 2020 selvamatic. All rights reserved.
//

import Foundation


class UsersManager: NSObject {
    
    static let get  = UsersManager()
    var usersService = UserServices.get
    
    private override init() {
        
    }
    
    func getUser(cedula: String, responseValue: @escaping (UserRes) -> Void, onFailure: (() -> Void)? = nil){
             usersService.getUser(cedula: cedula, responseValue: responseValue, onFailure: onFailure)
         }
    
    func getUsersList(responseValue: @escaping ([UserRes]) -> Void, onFailure: (() -> Void)? = nil) {
        usersService.getUsersList(responseValue: responseValue, onFailure: onFailure)
    }
    
    func createUser(cedula: String, celular: String, direccion: String, email: String, nombre: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil){
        usersService.createUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre, responseValue: responseValue, onFailure: onFailure)
    }
    
    func editUser(cedula: String, celular: String, direccion: String, email: String, nombre: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil){
          usersService.editUser(cedula: cedula, celular: celular, direccion: direccion, email: email, nombre: nombre, responseValue: responseValue, onFailure: onFailure)
      }
    
    func deleteUser(cedula: String, responseValue: @escaping (GenericRes) -> Void, onFailure: (() -> Void)? = nil){
        usersService.deleteUser(cedula: cedula, responseValue: responseValue, onFailure: onFailure)
    }
    
    
}
